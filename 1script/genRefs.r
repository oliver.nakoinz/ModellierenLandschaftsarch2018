library(bibtex)
write.bib(c("base", "raster", "factoextra", "proxy", "tidyverse", "ggplot2", "dplyr", "readr", "tidyr", "rmarkdown", "knitr", "cluster"), file='../packageReferences')
