# http://www.gis-blog.com/r-raster-data-acquisition/


Obtaining site characteristics
setwd("/home/fon/daten/lehre/lv/lv36_Berlin2018/lvMod")
library(sp)
m_locations <- read.csv("2data/meg_dw.csv", sep = ";")
sp::coordinates(m_locations) <- ~x+y
sp::proj4string(m_locations) <- sp::CRS("+init=epsg:31467")
t_locations <- read.csv("2data/tum_dw.csv", sep = ";")
sp::coordinates(t_locations) <- ~x+y
sp::proj4string(t_locations) <- sp::CRS("+init=epsg:31467")

library(mapview)
mapview(m_locations) + t_locations


library(raster)
raster::getData('SRTM', lon=9, lat=54, path = "3geodata")
raster::getData('SRTM', lon=12, lat=54, path = "3geodata")
srtm1 <- raster("3geodata/srtm_38_02.tif")
srtm2 <- raster("3geodata/srtm_39_02.tif")
plot(srtm2)


de0 <- getData('GADM', country='DE', level=0)
plot(de0)
de1 <- getData('GADM' , country="DE", level=1)
de1$NAME_1
plot(de1[15,])

srtm=merge(srtm1, srtm2)
plot(srtm)
plot(de1, add=TRUE)
srtm=crop(srtm,de1[15,])
plot(srtm)
plot(de1[15,], add=TRUE)

srtm = projectRaster(srtm, crs = CRS("+init=epsg:31467"))

library(RColorBrewer)
pal <- colorRampPalette(brewer.pal(9, "BrBG"))
mapview(srtm, col.regions = pal(100), alpha=0.3, legend = TRUE) + m_locations

ter <- terrain(srtm, opt=c('slope', 'aspect', 'TPI'), unit='degrees')
plot(ter)

m_locations@data$alt <- extract(x = srtm, y = m_locations, buffer = 200, fun = mean)
m_locations@data$tpi <- extract(x = ter$tpi, y = m_locations, buffer = 200, fun = median)
m_locations@data$slope <- extract(x = ter$slope, y = m_locations, buffer = 200, fun = mean)
m_locations@data$aspect <- extract(x = ter$aspect, y = m_locations, buffer = 200, fun = mean)

t_locations@data$alt <- extract(x = srtm, y = t_locations, buffer = 200, fun = mean)
t_locations@data$tpi <- extract(x = ter$tpi, y = t_locations, buffer = 200, fun = median)
t_locations@data$slope <- extract(x = ter$slope, y = t_locations, buffer = 200, fun = mean)
t_locations@data$aspect <- extract(x = ter$aspect, y = t_locations, buffer = 200, fun = mean)


library(vioplot)
vioplot(m_locations@data$alt, t_locations@data$alt, names=c("M", "T"), col="gold")
vioplot(m_locations@data$tpi, t_locations@data$tpi, names=c("M", "T"), col="gold")
vioplot(m_locations@data$slope, t_locations@data$slope, names=c("M", "T"), col="gold")
vioplot(m_locations@data$aspect, t_locations@data$aspect, names=c("M", "T"), col="gold")







