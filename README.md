# Modellierung in der Landschaftsarchäologie

[PD Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)

email: oliver.nakoinz@ufg.uni-kiel.de

https://gitlab.com/oliver.nakoinz/ModellierenLandschaftsarch2018

## Ziel

Diese beiden LV haben zum Ziel Grundlagen der Theorie der Modellierung und der Modellierung in der Landschaftsarchäologie zu vermitteln. Die Inhalten werden hierbei gemeinsam erarbeitet und die Übung soll Arbeit an eingenen Daten in mehreren Arbeitsgruppen beinhalten. 

Die Materialien zur LV sind unter [https://gitlab.com/oliver.nakoinz/ModellierenLandschaftsarch2018](https://gitlab.com/oliver.nakoinz/ModellierenLandschaftsarch2018) verfügbar.

-----------------------------------------------

## Programm

### Veranstaltungen
- V: [Vorlesung Theorie landschaftsarchäologischer Modelle](http://www.fu-berlin.de/vv/de/lv/402731?query=l%FCcke&sm=344910)
- S: [Seminar Praxis der Modellierung in der Landschaftsarchäologie](http://www.fu-berlin.de/vv/de/lv/402732?query=nakoinz&sm=344910)

### Zeitplan

- Montag 26.02.2018
    - 09:00-13:00 V Einführung in die Modellierung
        - Bergrüßung
        - [Einführung in die Modellierung](9praes/1mo/modelling_praes_v01.html) oder 
[Einführung in die Modellierung online](http://scientific-title.surge.sh/praes_modelling.html)
    - 14:00-18:00 S Einführung in R
        - [Einführung in R](9praes/1mo/r-slides.pdf) 
        - [Ergänzung zur Einführung in R](9praes/1mo/R2.html) oder [Ergänzung zur Einführung in R online](http://scientific-title.surge.sh/R2.html)
        - [Reproducible Research](9praes/1mo/RepResPraes_v01.html) oder [Reproducible Research online](http://scientific-title.surge.sh/RepResPraes_v01.html)
        - Eigene Fallstudien und Formierung von Arbeitsgruppen
- Dienstag 27.02.2018
    - 09:00-13:00 V Theorie der Interaktionsmodellierung (Exkurs: Dichte)
        - [Theorie der Interaktionsmodellierung](9praes/2di/1nakoinz_paris2017b_v02a.pdf) 
        - [Density](9praes/2di/density.html) oder [Density online](http://scientific-title.surge.sh/density.html)
    - 14:00-18:00 S Praxis  der Interaktionsmodellierung
        - [Praxis  der Interaktionsmodellierung](9praes/2di/3qaam1_c10p_interaction.html)
        - [Density](9praes/2di/2qaam1_c4p_v01_dichte.pdf)
        - Arbeit an Fallstudien
- Mittwoch 28.02.2018
    - 09:00-13:00 V Theorie der Wegemodellierung
        - [Theorie der Wegemodellierung](9praes/3mi/1qaam1_c9t_netzwerk.pdf) 
    - 14:00-18:00 S Praxis der Wegemodellierung
        - [Obtaining SRTM data](9praes/3mi/srtm.html) oder [Obtaining SRTM data  online](http://scientific-title.surge.sh/srtm.html)   
        - [Praxis der Wegemodellierung](9praes/3mi/2qaam1_c9p_netzwerk.html) 
        - Arbeit an Fallstudien
- Donnerstag 01.03.2018
    - 09:00-13:00 V Theorie des Predictive Modelling (Exkurs: Regression)
        - [Prädiktionskonzepte](9praes/4do/PredictionConcepts.html) oder [Prädiktionskonzepte online](http://scientific-title.surge.sh/PredictionConcepts.html)
        - [Regression](9praes/4do/1Regressionsmodelle.html) oder [Regression online](http://scientific-title.surge.sh/tutorial_regessionmodels.html)
        - [Theorie des Predictive Modelling](9praes/4do/2qaam1_c6t_loc.pdf)
    - 14:00-18:00 S Praxis des  Predictive Modelling
        - [Praxis des  Predictive Modelling](9praes/4do/4qaam1_c6p_loc.html) oder [Praxis des  Predictive Modelling online](http://scientific-title.surge.sh/4qaam1_c6p_loc.html) 
        - Arbeit an Fallstudien
- Freitag 02.03.2018
    - 09:00-13:00 S Fallstudien
        - Arbeit an Fallstudien
        - Berichte zu den Fallstudien
    - 14:00-15:00 V Synthese Modellierung in der Landschaftsarchäologie
        - Synthese
        - [Ausblick](9praes/5fr/AusblickModellierung.html) oder [Ausblick online](http://scientific-title.surge.sh/AusblickModellierung.html)
        - Abschied
-----------------------------------------------

Der nächste Schritt:

- [MOSAICnet](MOSAICnet_Networks_in_Archaeological_Res.pdf) oder [MOSAICnet academia](https://www.academia.edu/35958767/MOSAICnet_Networks_in_Archaeological_Research) und die [Anmeldeformulare](https://www.ufg.uni-kiel.de/de/aktuelles/events/tagungen-ausstellungen/mosaic2018)

-----------------------------------------------

## Daten, Skript und Literatur

Literaturempfehlung: 

- [QAAM1](http://www.springer.com/de/book/9783319295367) 
- [QAAM1 FU](https://fu-berlin.hosted.exlibrisgroup.com/primo_library/libweb/action/display.do;jsessionid=4154B809F4EC8A6AB8C04F1CD419BDBA?tabs=detailsTab&ct=display&fn=search&doc=FUB_ALMA_DS51960803280002883&indx=2&recIds=FUB_ALMA_DS51960803280002883&recIdxs=1&elementId=1&renderMode=poppedOut&displayMode=full&frbrVersion=&pcAvailabiltyMode=false&&query=any%2Ccontains%2Cnakoinz+knitter&dscnt=0&search_scope=FUB_Blended_2&vl(1UIStartWith0)=contains&scp.scps=scope%3A%28%22FUB_SEM%22%29%2Cscope%3A%28FUB_FUEM%29%2Cscope%3A%28FUB_ALMA%29%2Cscope%3A%28%22FUB_DBIS%22%29%2Cprimo_central_multiple_fe&vid=FUB&mode=Basic&highlight=true&institution=FUB&bulkSize=10&tab=fub&displayField=all&fromLocation=CMS&vl(freeText0)=nakoinz%20knitter&dum=true&vl(277659841UI0)=any&dstmp=1518351372451)- 
- [r4ds](http://r4ds.had.co.nz/)
- [Applied Spatial Data Analysis with R](http://www.springer.com/de/book/9781461476177)
- [Applied Spatial Data Analysis with R (Datei 1. Auflage)](http://gis.humboldt.edu/OLM/r/Spatial%20Analysis%20With%20R.pdf)
- [Reproducible Research](https://link.springer.com/article/10.1007%2Fs10816-015-9272-9)
- [geting spatial data](http://www.gis-blog.com/r-raster-data-acquisition/)
- [Statistical Learning with R](http://www-bcf.usc.edu/~gareth/ISL/ISLR%20First%20Printing.pdf)
- [R in Action](ftp://ftp.micronet-rostov.ru/linux-support/books/programming/R/Kabacoff%20Robert%20I.%20-%20R%20in%20Action%20-%202011.pdf)
- [How-To-Do-Archaeological-Science-Using-R](https://benmarwick.github.io/How-To-Do-Archaeological-Science-Using-R/)

R Cheat Sheets etc.

- [RStudio R Base](https://www.rstudio.com/wp-content/uploads/2016/10/r-cheat-sheet-3.pdf)
- [RStudio all CheatSheets](https://www.rstudio.com/resources/cheatsheets/)
- [R short refcard](https://cran.r-project.org/doc/contrib/Short-refcard.pdf)
- [R refcard](https://cran.r-project.org/doc/contrib/Baggott-refcard-v2.pdf)
- [Cran Task View Archaeology](https://github.com/benmarwick/ctv-archaeology)

Daten und Skripte:

- [QAAM1](https://github.com/dakni/mhbil)
- [gitlab skripte](https://gitlab.com/oliver.nakoinz/ModellierenLandschaftsarch2018/tree/master/1script)
- [gitlab daten](https://gitlab.com/oliver.nakoinz/ModellierenLandschaftsarch2018/tree/master/2data)
- [gitlab daten](https://gitlab.com/oliver.nakoinz/ModellierenLandschaftsarch2018/tree/master/9praes)


Code-Etherpad

- [LandschaftsarchModelle2018Berlin](https://pad.systemli.org/p/LandschaftsarchModelle2018Berlin)

-----------------------------------------------

